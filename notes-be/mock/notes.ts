type NoteType = {
  id: number;
  author: string;
  authorId: number;
  text: string;
  color: string;
  position: { x: number; y: number };
};

export const notesArray: Array<NoteType> = [];
