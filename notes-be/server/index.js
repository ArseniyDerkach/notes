"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const http_1 = __importDefault(require("http"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const cors_1 = __importDefault(require("cors"));
const socket_io_1 = __importDefault(require("socket.io"));
const notes_1 = require("../mock/notes");
const users_1 = require("../mock/users");
const JWT_SECRET = "be274d4013154ea789846763c119fc8492b702446e4f265b50e628e494918d23";
const verifyToken = (req, res, next) => {
    const authHeader = req.headers.authorization;
    if (!authHeader) {
        return res.status(401).json({ message: "Unauthorized" });
    }
    const token = authHeader.split(" ")[1];
    if (!token) {
        return res.status(401).json({ message: "Unauthorized" });
    }
    try {
        const decoded = jsonwebtoken_1.default.verify(token, JWT_SECRET);
        req.user = decoded;
        next();
    }
    catch (err) {
        return res.status(401).json({ message: "Unauthorized" });
    }
};
const app = (0, express_1.default)();
app.use((0, cors_1.default)());
app.use(express_1.default.json());
const server = http_1.default.createServer(app);
app.use((req, res, next) => {
    if (req.path === "/login") {
        next();
    }
    else {
        verifyToken(req, res, next);
    }
});
const io = new socket_io_1.default.Server(server, {
    cors: { origin: "http://localhost:3000", methods: ["GET", "POST", "PATCH"] },
});
app.post("/login", (req, res) => {
    const { name } = req.body;
    const token = jsonwebtoken_1.default.sign({ name }, JWT_SECRET);
    const id = Date.now();
    res.json({ message: "Logged in successfully", token, name, id });
    const user = {
        id,
        name,
    };
    users_1.usersArray.push(user);
});
app.get("/users/:userId", (req, res) => {
    const userId = +req.params.userId;
    const user = users_1.usersArray.find((user) => user.id === userId);
    if (user) {
        res.send(user);
    }
    else {
        res.status(404).send("User not found");
    }
});
app.post("/note", (req, res) => {
    notes_1.notesArray.push(req.body);
    res.send(req.body);
    io.emit("notes", JSON.stringify(notes_1.notesArray));
});
app.patch("/note", (req, res) => {
    const { id, text, position } = req.body;
    const currentNote = notes_1.notesArray.find((note) => note.id === id);
    if (!currentNote)
        return;
    if (text) {
        currentNote.text = text;
    }
    if (typeof position !== "undefined") {
        currentNote.position = Object.assign({}, position);
    }
    io.emit("notes", JSON.stringify(notes_1.notesArray));
    res.send(req.body);
});
app.get("/notes", (req, res) => {
    res.send(notes_1.notesArray);
});
io.on("connection", (socket) => {
    socket.broadcast.emit("notes", JSON.stringify(notes_1.notesArray));
    socket.on("get_notes", () => {
        socket.broadcast.emit("notes", JSON.stringify(notes_1.notesArray));
    });
});
const port = process.env.PORT || "3001";
server.listen(port, () => {
    console.log(`Server started on port ${port}`);
});
