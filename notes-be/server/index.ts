import express, { Application, Request, Response, NextFunction } from "express";
import http, { Server } from "http";
import jwt from "jsonwebtoken";
import cors from "cors";
import socketIo, { Socket } from "socket.io";
import { notesArray } from "../mock/notes";
import { usersArray } from "../mock/users";

const JWT_SECRET =
  "be274d4013154ea789846763c119fc8492b702446e4f265b50e628e494918d23";

interface AuthenticatedRequest extends Request {
  user: { [key: string]: any };
}
const verifyToken = (
  req: AuthenticatedRequest,
  res: Response,
  next: NextFunction
) => {
  const authHeader = req.headers.authorization;
  if (!authHeader) {
    return res.status(401).json({ message: "Unauthorized" });
  }

  const token = authHeader.split(" ")[1];
  if (!token) {
    return res.status(401).json({ message: "Unauthorized" });
  }

  try {
    const decoded = jwt.verify(token, JWT_SECRET) as { [key: string]: any };
    req.user = decoded;
    next();
  } catch (err) {
    return res.status(401).json({ message: "Unauthorized" });
  }
};

const app: Application = express();

app.use(cors());

app.use(express.json());

const server: Server = http.createServer(app);

app.use((req: Request, res: Response, next: NextFunction) => {
  if (req.path === "/login") {
    next();
  } else {
    verifyToken(req as AuthenticatedRequest, res, next);
  }
});

const io: socketIo.Server = new socketIo.Server(server, {
  cors: { origin: "http://localhost:3000", methods: ["GET", "POST", "PATCH"] },
});

app.post("/login", (req: Request, res: Response) => {
  const { name } = req.body;
  const token = jwt.sign({ name }, JWT_SECRET);
  const id = Date.now();
  res.json({ message: "Logged in successfully", token, name, id });
  const user = {
    id,
    name,
  };
  usersArray.push(user);
});
app.get("/users/:userId", (req, res) => {
  const userId = +req.params.userId;

  const user = usersArray.find((user) => user.id === userId);

  if (user) {
    res.send(user);
  } else {
    res.status(404).send("User not found");
  }
});

app.post("/note", (req: Request, res: Response) => {
  notesArray.push(req.body);
  res.send(req.body);
  io.emit("notes", JSON.stringify(notesArray));
});

app.patch("/note", (req: Request, res: Response) => {
  const { id, text, position } = req.body;
  const currentNote = notesArray.find((note) => note.id === id);
  if (!currentNote) return;
  if (text) {
    currentNote.text = text;
  }
  if (typeof position !== "undefined") {
    currentNote.position = { ...position };
  }
  io.emit("notes", JSON.stringify(notesArray));
  res.send(req.body);
});

app.get("/notes", (req: Request, res: Response) => {
  res.send(notesArray);
});

io.on("connection", (socket: Socket) => {
  socket.broadcast.emit("notes", JSON.stringify(notesArray));
  socket.on("get_notes", () => {
    socket.broadcast.emit("notes", JSON.stringify(notesArray));
  });
});
const port = process.env.PORT || "3001";

server.listen(port, () => {
  console.log(`Server started on port ${port}`);
});
