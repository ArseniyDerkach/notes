import React from "react";
import { Home } from "./pages";

import { useSelector } from "react-redux";
import { setAuth } from "./redux/auth.slice";
import { RootState } from "./redux/store";
import LoginPopup from "./components/login-popup";

function App() {
  const isAuthenticated = useSelector(
    (state: RootState) => state.auth.isAuthenticated
  );
  const token = localStorage.getItem("token");
  return token || isAuthenticated ? <Home /> : <LoginPopup />;
}

export default App;
