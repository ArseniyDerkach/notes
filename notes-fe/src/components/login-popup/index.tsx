import { useLoginMutation } from "../../redux/api.slice/api.slice";
import { useRef } from "react";
import { useDispatch } from "react-redux";
import { setAuth } from "../../redux/auth.slice";

export default function LoginPopup() {
  const dispatch = useDispatch();
  const inputRef = useRef<HTMLInputElement>(null);
  const [login] = useLoginMutation();

  async function handleLogin() {
    if (inputRef.current) {
      const value = inputRef.current.value;
      const response = await login(value);
      if ("error" in response) {
        console.log(response.error);
      } else {
        const { id, name } = response.data;
        const token = response.data.token;
        localStorage.setItem("token", token);
        localStorage.setItem("authorId", id);
        dispatch(setAuth({ id, user: name }));
      }
    }
  }
  return (
    <div>
      <input type="text" placeholder="your name" ref={inputRef} />
      <button onClick={handleLogin}>login</button>
    </div>
  );
}
