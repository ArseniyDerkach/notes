import { useState, useRef, useEffect } from "react";
import sanitizeHtml from "sanitize-html";
import ContentEditable, { ContentEditableEvent } from "react-contenteditable";
import { useDebounce } from "../../hooks";
import Draggable, { DraggableData, DraggableEvent } from "react-draggable";
import styles from "./Note.module.css";
import { useEditNoteMutation } from "../../redux/api.slice/api.slice";
import { NoteType } from "../../types/note";

export default function Note(props: NoteType) {
  const { id, author, authorId, text, color, position, currentAuthorId } =
    props;
  const [editNote] = useEditNoteMutation();

  const [content, setContent] = useState(text);
  const [currentPosition, setCurrentPosition] = useState(position);
  const contentEditable = useRef(null);
  const nodeRef = useRef(null);
  function handleChange(event: ContentEditableEvent) {
    const sanitizeConf = {
      allowedTags: [],
      allowedAttributes: {},
    };
    setContent(sanitizeHtml(event.currentTarget.innerText, sanitizeConf));
  }
  useEffect(() => {
    setContent(text);
    setCurrentPosition(position);
  }, [text, position.x, position.y]);

  const debouncedContent = useDebounce(content, 500);

  useEffect(() => {
    if (text === debouncedContent) return;
    editNote({ id, text: debouncedContent });
  }, [debouncedContent]);

  function handleControlledDrag(e: DraggableEvent, data: DraggableData) {
    const { x, y } = data;
    setCurrentPosition({ x, y });
  }
  function handleDragEnd(e: DraggableEvent, data: DraggableData) {
    const { x, y } = data;
    setCurrentPosition({ x, y });
  }

  const debouncedPosition = useDebounce(currentPosition, 500);

  useEffect(() => {
    if (
      position.x === debouncedPosition.x &&
      position.y === debouncedPosition.y
    )
      return;
    editNote({ id, position: debouncedPosition });
  }, [debouncedPosition]);

  return (
    <Draggable
      bounds="parent"
      disabled={authorId !== currentAuthorId}
      handle=".drag-zone"
      defaultPosition={{ x: position.x, y: position.y }}
      position={{ x: currentPosition.x, y: currentPosition.y }}
      scale={1}
      onDrag={handleControlledDrag}
      onStop={handleDragEnd}
      nodeRef={nodeRef}
    >
      <div
        ref={nodeRef}
        className={`${styles.note} no-cursor`}
        style={{
          backgroundColor: color,
          border: authorId === currentAuthorId ? "2px solid #555" : "none",
          borderRadius: 5,
        }}
      >
        <div className={`${styles.noteAuthor} drag-zone`}>{`${author}`}</div>
        <ContentEditable
          className={styles.noteText}
          innerRef={contentEditable}
          html={content}
          disabled={false}
          onChange={handleChange}
        />
      </div>
    </Draggable>
  );
}
