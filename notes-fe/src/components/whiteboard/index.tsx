import Note from "../note";
import { NoteType } from "../../types/note";
import { useState, useEffect } from "react";
import styles from "./Whiteboard.module.css";
import {
  useGetNotesQuery,
  useAddNoteMutation,
  useGetUserQuery,
} from "../../redux/api.slice/api.slice";
import { useSelector } from "react-redux";
import { RootState } from "../../redux/store";

export default function Whiteboard() {
  const { data } = useGetNotesQuery("");
  const [addNote] = useAddNoteMutation();
  const [currentAuthor, setCurrentAuthor] = useState("");
  const [currentAuthorId, setCurrentAuthorId] = useState(
    Number(localStorage.getItem("authorId")) || 0
  );
  const { user: currentUser, id } = useSelector(
    (state: RootState) => state.auth
  );
  const { data: userData } = useGetUserQuery(currentAuthorId);

  useEffect(() => {
    if (userData?.id) {
      setCurrentAuthorId(userData.id);
    }
    if (userData?.name) {
      setCurrentAuthor(userData.name);
    }
  }, [userData]);

  const notesColors: Array<string> = [
    "#ffe66e",
    "#a0ef9b",
    "#feafdf",
    "#d7b0ff",
    "#9edfff",
    "#e0e0e0",
  ];

  function handleAddNote(event: React.MouseEvent<HTMLDivElement>) {
    if (event.target !== event.currentTarget) return true;
    const newNote = {
      id: Date.now(),
      text: "",
      author: currentAuthor,
      authorId: currentAuthorId,
      position: {
        x: event.clientX - 100,
        y: event.clientY - 100,
      },
      color: notesColors[Math.floor(Math.random() * 6)],
    };
    addNote(newNote);
  }
  return (
    <div className={styles.whiteboard} onClick={handleAddNote}>
      <h1>whiteboard</h1>
      {data &&
        data.map((note: NoteType) => (
          <Note key={note.id} {...note} currentAuthorId={currentAuthorId} />
        ))}
    </div>
  );
}
