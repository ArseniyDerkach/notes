import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import io from "socket.io-client";

export const notesApi = createApi({
  reducerPath: "api",

  baseQuery: fetchBaseQuery({
    baseUrl: "http://localhost:3001/",
    prepareHeaders: (headers, { getState }) => {
      const token = localStorage.getItem("token");

      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }

      return headers;
    },
  }),
  endpoints: (builder) => ({
    login: builder.mutation({
      query: (name) => ({
        url: "/login",
        method: "POST",
        body: { name },
      }),
    }),
    getUser: builder.query({
      query: (id) => ({
        url: `/users/${id}`,
      }),
    }),
    addNote: builder.mutation({
      query: (note) => ({
        url: "/note",
        method: "POST",
        body: note,
      }),
    }),
    editNote: builder.mutation({
      query: (fields) => ({
        url: "/note",
        method: "PATCH",
        body: fields,
      }),
    }),
    getNotes: builder.query({
      query: () => "/notes",
      async onCacheEntryAdded(
        arg,
        { updateCachedData, cacheDataLoaded, cacheEntryRemoved }
      ) {
        const socket = io("http://localhost:3001", {
          transports: ["websocket", "polling", "flashsocket"],
        });
        socket.on("connect", () => {
          socket.emit("get_notes");
        });
        try {
          const cache = await cacheDataLoaded;
          const listener = (data: string) => {
            const parsedData = JSON.parse(data);
            updateCachedData((draft) => {
              draft.splice(0, draft.length);
              draft.push(...parsedData);
            });
          };
          socket.on("notes", listener);
        } catch (error) {
          console.error(error);
          throw new Error("Failed to get notes");
        }

        await cacheEntryRemoved;
        socket.close();
      },
    }),
  }),
});

export const {
  useGetNotesQuery,
  useGetUserQuery,
  useAddNoteMutation,
  useEditNoteMutation,
  useLoginMutation,
} = notesApi;
