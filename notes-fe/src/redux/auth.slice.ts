import type { PayloadAction } from "@reduxjs/toolkit";
import { createSlice } from "@reduxjs/toolkit";

type AuthState = {
  isAuthenticated: boolean;
  user?: string;
  id?: number;
};

const authSlice = createSlice({
  name: "auth",
  initialState: { isAuthenticated: false } as AuthState,
  reducers: {
    setAuth: (state, action: PayloadAction<{ id: number; user: string }>) => {
      state.isAuthenticated = true;
      state.user = action.payload.user;
      state.id = action.payload.id;
    },
  },
});

export const { setAuth } = authSlice.actions;

export default authSlice;
