import { configureStore } from "@reduxjs/toolkit";

import { notesApi } from "./api.slice/api.slice";
import authSlice from "./auth.slice";

export const store = configureStore({
  reducer: {
    [authSlice.name]: authSlice.reducer,
    [notesApi.reducerPath]: notesApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(notesApi.middleware),
});

export type RootState = ReturnType<typeof store.getState>;
