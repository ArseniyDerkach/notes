# Notes application

### Stack:
- Frontend: React+TS+Redux toolkit
- Backend: Node.js+TS

### How to launch
- Frontend: `npm start`, serves on port 3000
- Backend: `npm run dev`, serves on port 3001

### Details
- Backend saves data to local variables, so restarting server will delete all your notes
- All auth process is made just by name, after that token and userId is placed in localstorage.
- The same names could be different persons, unique identifier is userId